# import os

# def run_python_program(program_name):
#     os.system("gnome-terminal -e 'bash -c \"python " + program_name + "\"'")


# print ("Levantando el microservicio sv_gestor_tweets.py")
# run_python_program('servicios/get_content.py')

# print ("Levantando el microservicio sv_gestor_tweets.py")
# run_python_program('servicios/get_content_id.py')
from servicios.get_content import appContent
from servicios.content_filter import appFilter
from servicios.get_content_id import app_id
from flask_api import FlaskAPI

app = FlaskAPI(__name__)
app.register_blueprint(appContent)
app.register_blueprint(appFilter)
app.register_blueprint(app_id)

if __name__ == "__main__":
    app.run(host='192.168.33.10',port='8000', debug=True)