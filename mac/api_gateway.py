# Tarea 2 - Arquitectura de Microservicios
# Archivo: api_gateway.py
# Autor(es): Manuel Sánchez, José Hernández, Viridiana Murillo & Diego López.
# Descripción: Es la interfaz de comunicación entre los microservicios y el usuario, para que estas don

# Descripcion de los elementos:
# Responsabilidad:
#   - Ofrece una interfaz de comunicación entre los microservicios del sistema y el usuario.
# Propiedades:
#   - Se comunica con los servicios content_get_filter, content_post y content_get_patch.

from flask_api import FlaskAPI, status, exceptions
from flask import redirect

app = FlaskAPI(__name__)

# Ruta que llama al microservicio content_get_filter.
@app.route("/netflix/original-content", methods=['GET'])
def get_content():
    url = "http://192.168.33.10:8000/netflix/original-content"
    return redirect(url,code=302,Response=None)

# Ruta que llama al microservicio content_get_patch
@app.route("/netflix/original-content/<int:key>", methods=['GET','PATCH'])
def get_content_id(key):
    url = "http://192.168.33.10:8001/netflix/original-content/"+str(key)
    return redirect(url,code=302,Response=None)


# Ruta que llamada al microservicio content_post
@app.route("/netflix/original-content", methods=['GET','POST'])
def content_post():
    url = "http://192.168.33.10:8002/netflix/original-content"
    return redirect(url,code=302,Response=None)


# Ejecucion de la API Gateway
if __name__ == "__main__":
    app.run(host='192.168.33.10', port='8003', debug=True)