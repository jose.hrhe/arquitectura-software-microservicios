# Tarea 2 - Arquitectura de Microservicios
# Archivo: content_get_put.py
# Autor(es): Manuel Sánchez, José Hernández, Viridiana Murillo & Diego López.
# Descripción: Es el microservicio que se comunica con la base de datos y actualiza una pelicula o serie en base al ID
# de dicho contenido.

from flask import request
from flask_api import FlaskAPI, status, exceptions
from models import Contenido

import sqlite3

app = FlaskAPI(__name__)

# Ruta para actualizar contenido especifico de netlfix.
@app.route("/netflix/original-content/<int:key>", methods=['GET','PATCH'])
def content_details(key):
    """
    Información de un contenido en particular.
    """
    if request.method == 'GET':
        sqlite_conn = get_database_connection('./mac/original_content.db')
        sqlite_cursor = sqlite_conn.cursor()
        sqlite_cursor.execute('SELECT * FROM original_content WHERE id ='+str(key))
        data = sqlite_cursor.fetchall()
        sqlite_conn.close()
        result = convert_cursor_to_json(data)
        return result

    if request.method == 'PATCH':
        title = str(request.data.get('name',''))
        tipo = str(request.data.get('type', ''))
        genre = str(request.data.get('genre', ''))
        imdb_rating = str(request.data.get('imdb_rating', ''))
        objeto_contenido = create_object(key)
        new_object(objeto_contenido,title,tipo,genre,imdb_rating)
        update_database(objeto_contenido,key)
        sqlite_conn = get_database_connection('./mac/original_content.db')
        sqlite_cursor = sqlite_conn.cursor()
        sqlite_cursor.execute('SELECT * FROM original_content WHERE id ='+str(key))
        data = sqlite_cursor.fetchall()
        sqlite_conn.close()
        result = convert_cursor_to_json(data)
        return result

# Consulta que actualizar datos en la base de datos con los nuevos datos.
def update_database(data,key):
    sqlite_conn = get_database_connection('./mac/original_content.db')
    tupla_sql = (data.getNombre(),data.getTipo(),data.getGenero(),data.getRating(),key)
    sqlite_cursor = sqlite_conn.cursor()
    sql = "UPDATE original_content SET name=?,type=?,genre=?,imdb_rating=? WHERE id = ?"
    sqlite_cursor.execute(sql,tupla_sql)
    sqlite_conn.commit()
    last_id = sqlite_cursor.lastrowid
    sqlite_conn.close()


# Crea un objecto contenido para actualizarlo en la base de datos.
def create_object(key):
    sqlite_conn = get_database_connection('./mac/original_content.db')
    sqlite_cursor = sqlite_conn.cursor()
    sqlite_cursor.execute('SELECT * FROM original_content WHERE id ='+str(key))
    data = sqlite_cursor.fetchall()
    sqlite_conn.close()
    contenido = get_data(data)
    return contenido

# Crea un objeto Contenido ingresando información nueva.
def get_data(data):
    contenido = Contenido()
    contador = 0
    for e in data:
        contenido.setNombre(e[1])
        contenido.setTipo(e[2])
        contenido.setGenero(e[3])
        contenido.setRating(e[4])
    return contenido

# Cambia los atributos del objeto contenido para posteriormente actualizarlo en la base de datos.
def new_object(objeto_contenido,title,tipo,genre,imdb_rating):
    if title != "":
        objeto_contenido.setNombre(title)
    if tipo != "":
        objeto_contenido.setTipo(tipo)
    if genre != "":
        objeto_contenido.setGenero(genre)
    if imdb_rating != "":
        objeto_contenido.setRating(float(imdb_rating))

# Se obtiene la conexion a la base de datos.
def get_database_connection(database_path):
    conn = sqlite3.connect(database_path)
    return conn

# Convierte la información extraida de la base de datos a JSON.
def convert_cursor_to_json(cursor_data):
    result_list = []
    for e in cursor_data:
        temp_dict = {}
        temp_dict['id'] = e[0]
        temp_dict['name'] = e[1]
        temp_dict['type'] = e[2]
        temp_dict['genre'] = e[3]
        temp_dict['imdb_rating'] = e[4]
        temp_dict['url'] = "http://192.168.33.10:8001/netflix/original-content/"+str(e[0])
        result_list.append(temp_dict)
    return result_list

# Ejecución del microservicio content_get_put.
if __name__ == "__main__":
    app.run(host='192.168.33.10',port='8001', debug=True)