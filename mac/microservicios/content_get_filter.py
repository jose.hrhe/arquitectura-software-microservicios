# Tarea 2 - Arquitectura de Microservicios
# Archivo: content_post.py
# Autor(es): Manuel Sánchez, José Hernández, Viridiana Murillo & Diego López.
# Descripción: Es el microservicio que se comunica con la base de datos y muestra el contenido original de netlfix 
# y tambien realiza busquedas con filtro en base a ciertos parametros.

from flask import request
from flask_api import FlaskAPI, status, exceptions

import sqlite3
import json

app = FlaskAPI(__name__)

# Ruta para consultar todo el contenido original de netlfix.
@app.route("/netflix/original-content", methods=['GET'])
def content_netflix():
    """
    Muestra el contenido orignal de netflix
    """
    if request.method == 'GET':
        tipo = request.args.get('type',type = str)
        genero = request.args.get('genre',type = str)
        popularidad = request.args.get('imdb_rating',type = str)

        sqlite_conn = get_database_connection('./mac/original_content.db')
        sqlite_cursor = sqlite_conn.cursor()

        if tipo is not None:
            tipo = tipo.replace("'","").lower()
        if genero is not None:
            genero = genero.replace("'","").capitalize()
        
        sql = create_query(tipo,genero,popularidad)
        sqlite_cursor.execute(sql)
        data = sqlite_cursor.fetchall()
        sqlite_conn.close()
        result = convert_cursor_to_json(data)
        return result

# Crea una consulta con diferentes valores en base a los parametros de busqueda.
def create_query(tipo,genero,popularidad):
    sql = "SELECT * FROM original_content "
    if tipo is not None and genero is not None and popularidad is not None:
        sql = sql + "WHERE type='"+tipo+"' AND genre LIKE '%"+genero+"%' AND imdb_rating='"+popularidad+"'"
    elif tipo is not None and genero is not None and popularidad is None:
        sql = sql + "WHERE type='"+tipo+"' AND genre LIKE '%"+genero+"%'"
    elif tipo is not None and genero is None and popularidad is not None:
        sql = sql + "WHERE type='"+tipo+"' AND imdb_rating='"+popularidad+"'"
    elif tipo is None and genero is not None and popularidad is not None:
        sql = sql + "WHERE genre LIKE '%"+genero+"%' AND imdb_rating='"+popularidad+"'"
    elif tipo is not None and genero is None and popularidad is None:
        sql = sql + "WHERE type='"+tipo+"'"
    elif tipo is None and genero is not None and popularidad is None:
        sql = sql + "WHERE genre LIKE '%"+genero+"%'"
    elif tipo is None and genero is None and popularidad is not None:
        sql = sql + "WHERE imdb_rating='"+popularidad+"'"
    return sql

# Se obtiene la conexion a la base de datos.
def get_database_connection(database_path):
    conn = sqlite3.connect(database_path)
    return conn
    
# Convierte la información extraida de la base de datos a JSON.
def convert_cursor_to_json(cursor_data):
    result_list = []
    for e in cursor_data:
        temp_dict = {}
        temp_dict['id'] = e[0]
        temp_dict['name'] = e[1]
        temp_dict['type'] = e[2]
        temp_dict['genre'] = e[3]
        temp_dict['imdb_rating'] = e[4]
        temp_dict['url'] = "http://192.168.33.10:8001/netflix/original-content/"+str(e[0])
        result_list.append(temp_dict)
    return result_list

# Ejecución del microservicio content_get_filter.
if __name__ == "__main__":
    app.run(host='192.168.33.10',port='8000', debug=True)