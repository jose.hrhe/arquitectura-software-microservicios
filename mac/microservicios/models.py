# Tarea 2 - Arquitectura de Microservicios
# Archivo: models.py
# Autor(es): Manuel Sánchez, José Hernández, Viridiana Murillo & Diego López.
# Descripción: Es un modelo que guarda la información necesaria del contenido original de Netflix. 


class Contenido():
    
    # Inicializador de la clase Contenido
    # - nombre: Es el nombre de la pelicula o serie.
    # - tipo: Es el tipo al que pertence el contenido (movie,series)
    # - genero: Es el genero al que pertenece el contenido.
    # - imdb_rating: Es la califiación obtenida del contenido.
    def __init__(self):
        self.nombre = None
        self.tipo = None
        self.genero = None
        self.imdb_rating = None

    # Obtiene el nombre del contenido.
    def getNombre(self):
        return self.nombre

    # Cambia el nombre del contenido por uno nuevo.
    # - nombre: Es el nuevo nombre del contenido.
    def setNombre(self,nombre):
        self.nombre = nombre

    # Obtiene el tipo de contenido.
    def getTipo(self):
        return self.tipo
        
    # Cambia el tipo de contenido por uno nuevo.
    # - nombre: Es el nuevo tipo de contenido.
    def setTipo(self,tipo):
        self.tipo = tipo

    # Obtiene le genero del contenido.
    def getGenero(self):
        return self.genero

    # Cambia el genero del contenido por uno nuevo.
    # - nombre: Es el nuevo genero del contenido.
    def setGenero(self,genero):
        self.genero = genero

    # Obtiene la calificación del contenido.
    def getRating(self):
        return self.imdb_rating

    # Cambia la califiación del contenido por una califiación.
    # - nombre: Es la nueva calificación.
    def setRating(self, imdb_rating):
        self.imdb_rating = imdb_rating

    # Es el formato de salida cuando se muestra el objeto contenido.
    def __str__(self):
        return "Titulo: {0}, Tipo: {1}, Genero: {2}, Rating: {3}".format(self.nombre,self.tipo,self.genero,self.imdb_rating)