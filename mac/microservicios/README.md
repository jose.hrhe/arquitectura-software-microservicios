# Microservicios
En esta carpeta se definen los microservicios utilizados para el Sistema
de Administración de contenido original de Netflix.
Las especificaciones de todos los 
servicios se realizaron utilizando OpenAPI. Los microservicois y 
sus especificaciones aparecen a continuación.

---

# Procesador de consultas y filtrado de contenido original de Netflix.

Es un microservicio que permite a los usuarios obtener información de series o
películas de Netflix por medio de una Base de Datos.

## Servicio de Información [/netflix/original-content]

### Obtener infomación [GET]

+ Response 200 (application/json)

        { 
            "id": "texto",
            "name": "texto",
            "type": "texto",
            "genre": "texto",
            "imdb_rating": "texto",
        }

Ejemplo de uso: 
1. Ejecutar el sistema.
2. Ingresar a la dirección: 
http://192.168.33.10:8000/netflix/original-content

## Servicio de Información [/netflix/original-content?type='string'&genre='string'&imdb_rating='string']

+ Parameters
    + type - Corresponde al título de la película o serie de Netflix.
    + genre - Corresponde al genero de la película o serie de Netflix.
    + imdb_rating - Corresponde a la calificación de la película o serie de Netflix.

### Obtener películas o series [GET]

+ Response 200 (application/json)

        { 
            "id": "texto",
            "name": "texto",
            "type": "texto",
            "genre": "texto",
            "imdb_rating": "texto",
        }

Ejemplo de uso: 
1. Ejecutar el sistema.
2. Ingresar a la dirección: 
http://192.168.33.10:8000/netflix/original-content?type='series'

---

# Procesador de actualizaciones de contenido original de Netflix.

Es un microservicio que permite a los usuarios ver la información de un único
contenido y actualizar ese registro , ingresando los datos en formato JSON.

## Servicio de información [/netflix/original-content/<int:key>]

+ Parameters
    + key - Corresponde a el ID del contenido.

### Actualizar peliculas o series [PATCH]

+ Response 200 (application/json)

        { 
            "id": "texto",
            "name": "texto",
            "type": "texto",
            "genre": "texto",
            "imdb_rating": "texto",
        }
 
Ejemplo de uso: 
1. Ejecutar el sistema.
2. Envir una petición por POST a la dirección 
http://192.168.33.10:8001/netflix/original-content/1 ingresando en el recuadro del PATCH
los nuevos datos en formato JSON.

En la siguiente imagenes se muestra como se deben ingresar los datos en formato JSON.
![Ejemplo de la ejecución del microservicio](docs/ejecucion_patch.PNG)

---

# Procesador de nuevo contenido original de Netflix.

Es un microservicio que permite a los usuarios ingresar el nombre de un película o serie
original de Netflix (comunicandose con la API de OMDb) extrayendo información complmentaria
al contenido y guardarla en la base de datos.

## Servicio de información [/netflix/original-content]

### Ingresar peliculas o series de Netflix [POST]

+ Response 200 (application/json)

        { 
            "id": "texto",
            "name": "texto",
            "type": "texto",
            "genre": "texto",
            "imdb_rating": "texto",
        }
 
Ejemplo de uso: 
1. Ejecutar el sistema.
2. Enviar una petición por POST a la dirección 
http://192.168.33.10:8003/netflix/original-content ingresando en el recuadro del POST
los nuevos datos en formato JSON.

En la siguiente imagenes se muestra como se deben ingresar los datos en formato JSON.
![Ejemplo de ejecución del microservicio](docs/ejecucion_post.PNG)