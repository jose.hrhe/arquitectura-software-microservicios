# Tarea 2 - Arquitectura de Microservicios
# Archivo: content_post.py
# Autor(es): Manuel Sánchez, José Hernández, Viridiana Murillo & Diego López.
# Descripción: Es el microservicio que realiza inserciones a la base de datos y se comunica con la API
# de OMDb para extrer la informacion de una pelicula o una serie.

from flask import request
from flask_api import FlaskAPI, status, exceptions
import urllib
from omdb import omdb_information

import sqlite3
import json

app = FlaskAPI(__name__)

# Ruta para insertar un nuevo registro de una pelicula o una serie.
@app.route("/netflix/original-content", methods=['GET','POST'])
def content_netflix():
    """
    Ingresa contenido original de netflix
    """
    if request.method == 'GET':
        return [{}]
    elif request.method == 'POST':
        content = str(request.data.get('name',''))
        replace_con = content.replace(" ","+")
        data = omdb_information(replace_con)
        raw = create_raw(data)
        content_id = insert_database(raw)
        json_data = convert_cursor_to_json(raw,content_id)

        return json_data,status.HTTP_201_CREATED

# Consulta que inserta datos en la base de datos.
def insert_database(data):
    sqlite_conn = get_database_connection('./mac/original_content.db')
    tupla_sql = (data[0],data[3],data[1],data[2])
    sqlite_cursor = sqlite_conn.cursor()

    sql = "INSERT INTO original_content (name,type,genre,imdb_rating) VALUES(?,?,?,?)"
    sqlite_cursor.execute(sql,tupla_sql)
    sqlite_conn.commit()
    last_id = sqlite_cursor.lastrowid
    sqlite_conn.close()
    return last_id

# Se crea una fila de inserción para posteriormente, insertar la fila en la base de datos.
def create_raw(data):
    datos = []
    for key in data:
        if key == "Title":
            datos.append(data[key])
        elif key == "Type":
            datos.append(data[key])
        elif key == "imdbRating":
            datos.append(float(data[key]))
        elif key == "Genre":
            datos.append(data[key])

    return datos

# Se obtiene la conexion a la base de datos.
def get_database_connection(database_path):
    conn = sqlite3.connect(database_path)
    return conn

# Convierte la información extraida de la base de datos a JSON.
def convert_cursor_to_json(cursor_data,content_id):
    temp_dict = {}
    temp_dict['id'] = content_id
    temp_dict['name'] = cursor_data[0]
    temp_dict['type'] = cursor_data[3]
    temp_dict['genre'] = cursor_data[1]
    temp_dict['imdb_rating'] = cursor_data[2]
    return temp_dict


# Ejecución del microservicio content_post.
if __name__ == "__main__":
    app.run(host='192.168.33.10', port='8002', debug=True)