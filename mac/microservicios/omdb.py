import json
import urllib

# Tarea 2 - Arquitectura de Microservicios
# Archivo: omdb.py
# Autor(es): Manuel Sánchez, José Hernández, Viridiana Murillo & Diego López.
# Descripción: Este archivo su funcion principal es conectarse a la API de OMDb, y extraer y cargar el JSON con informacion
#  en base al titulo de una pelicual o una serie y enviarlo al microservicio content_post.py.

def omdb_information(title):
    url = "http://www.omdbapi.com/?apikey=b5997119&t="+str(title)
    datos = urllib.request.urlopen(url)
    data = json.loads(datos.read())
    return data